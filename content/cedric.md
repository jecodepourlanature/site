---
author: "Nicolas"
date: 2018-06-02
title: Cedric
description: "Suivre l'activité de la base CEDRIC"
draft: false
---

CEDRIC est une base de données gérée par l'État, elle recense les installations soumises à autorisation ou déclaration.

Le suivi de cette base tel qu'elle est présentée aujourd'hui est fastidieux. Notre projet est consulter régulièrement le site du ministère à votre place et lorsqu'un changement est détecté sur un établissement vous avertir par mail.

Le projet entre dans sa phase béta et sera bientôt disponible.
