---
author: "Nicolas"
date: 2018-06-02
title: Le projet
description: "Présentation des objectifs de je code pour la nature"
draft: false
---

Un peu à l'image des [chatons](https://chatons.org/), l'idée est de proposer un ensemble de services informatique et ou un soutient aux associations de protection de l'environnement, aux naturalistes et de manière plus générale à toute personne dont l'action contribue à la prise en compte et la protection de l'environnement.

Alors pourquoi passer du temps a faire tourner des services ou développer des projets pour la protection de la Nature ? Déjà parce que si on maîtrise l'informatique et qu'on veut aider les naturalistes, on le fera pas en comptant des oiseaux quand on ne sait pas différencier une mouette d'un goéland.

Ensuite parce que quand on donne de son temps bénévolement, on participe a améliorer l'usage de l'enveloppe budgétaire pour qu'un maximum des ressources soient attribuées effectivement à la protection. 

On peut également améliorer des outils et ainsi faire gagner du temps aux équipes qui travaillent sur ces sujets.

Enfin, on peut aussi accélérer un projet en le portant très tôt, voir en amont de la phase de recherche de financements, en faisant un démonstrateur qui peut aussi participer au montage du dossier de financement en fournissant des valeurs de référence pour estimer les besoins.

Si quelqu'un souhaite porter un projet ici, tant que le projet s'inscrit dans le projet commun et que c'est compatible avec les ressources dont on dispose il devra pouvoir se lancer.

Chaque projet a son responsable qui peut coordonner les interventions des autres participants. Si un ou plusieurs participants, ont un soucis sur un projet et bien ils sont libre de dupliquer le projet et poursuive dans une autre direction.

Actuellement nous avons le statut d'[association de fait](https://www.associations.gouv.fr/1080-association-non-declaree.html), par la suite, quand le besoin viendra, on aura a déclarer une association.

En attendant si vous êtes une association de protection de la nature ou un d'étude vous pouvez déjà nous solliciter sur contact@jecodepourlanature.tk
