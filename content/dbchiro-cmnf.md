---
author: "JB"
date: 2018-09-17
title: "DBChiro - CMNF"
description: "Deploiement d'une instance DB Chiro et migration de la base de la CMNF"
draft: false
---

La CMNF souhaite migrer sa base Access vers [dbchiro](https://www.dbchiro.org/).

Les scripts de migrations sont prêts. La CMNF testera la base avec la saisie des observations chiro de l'hiver 2018-19.
Si le test est concluant, les données de la base accès seront importées.

Un toilettage de la base gîtes est à prévoir après la migration.

Instance déployée : https://cmnf.jecodepourlanature.tk/


