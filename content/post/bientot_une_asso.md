---
author: "Nicolas"
date: 2018-10-15
title: Bientôt une asso
description: "Le projet avance"
draft: false 
---

Le projet avance doucement mais surement, la rédaction de statuts a commencé, peut être une AG bientôt, le débat est ouvert.

On a également répondu à l'appel d'une association qui cherchait des prestataires pour son informatique. Un exercice interessant qui nous donné l'occasion de réfléchir à ce que pourrait être notre propre infrastructure et les services que l'on souhaite construire.
:
