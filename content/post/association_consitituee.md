---
author: "JB"
date: 2019-03-30
title: Association constituée
description: "Suite à l'AG constitutive, l'association a été déclarée en préfecture"
draft: false 
---

L'assemblée générale constitutive s'est réunie le 13/02/2019, le CA et le bureau ont été élus et l'association a été déclarée le 11/03/2019.
Plus que quelques démarches et nous pourront bientôt être pleinement opérationnels sur le plan administratif !

