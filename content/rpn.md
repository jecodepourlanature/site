---
author: "JB"
date: 2018-06-16
title: RPN 
description: "Rapport de prospections naturalistes"
draft: false
---

Application écrite en python, elle permet de générer à partir d'un export clicnat un rapport à partager sous forme de lien. Chaque lot de données est stocké dans une table au sein d'un geopackage.

Idées de composants de rapports :
- Observateurs (en gérant les obs multi-observateurs) 

- Communes, territoires prospectés (jointure avec les réferentiels adminExpress)

- Especes, liens vers les fiches CN et/ou MNHN

- Aggrégations diverses (obs/commune, effectifs, etc.)

- Graphiques (type Chart.js) des espèces, niveaux de menace, etc.

- Carte de obs

- ...